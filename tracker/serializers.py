from rest_framework import serializers

from .models.users import User
from .models.tasks import Task


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id', 'name', 'position'
        )


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = (
            'id', 'name', 'description', 'user', 'checking_user'
        )