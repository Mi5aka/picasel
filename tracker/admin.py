from django.contrib import admin
from .models.tasks import Task
from .models.users import User


class UserAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'name', 'position'
    )
    list_filter = ['position']
    search_fields = ['name', 'position']


class TaskAdmin(admin.ModelAdmin):
    empty_value_display = '-'
    list_display = (
        'id', 'name', 'user'
    )
    search_fields = ['name', 'user']


admin.site.register(User, UserAdmin)
admin.site.register(Task, TaskAdmin)
