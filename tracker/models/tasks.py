from django.db import models
from django.utils.translation import ugettext_lazy as _


class Task(models.Model):

    name = models.CharField(
        max_length=100, verbose_name=_('Название задачи')
    )
    description = models.TextField(
        verbose_name=_('Описание')
    )
    user = models.ForeignKey(
        'tracker.User', null=True, blank=True,
        verbose_name=_('Назначенный пользователь')
    )
    checking_user = models.ForeignKey(
        'tracker.User', related_name='+',
        null=True, blank=True,
        verbose_name=_('Проверяющий пользователь')
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Задача')
        verbose_name_plural = _('Задачи')
