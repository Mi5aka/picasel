from django.db import models
from django.utils.translation import ugettext_lazy as _


class User(models.Model):

    name = models.CharField(
        max_length=50, verbose_name=_('Имя')
    )
    position = models.CharField(
        max_length=50, verbose_name=_('Дожность')
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Пользователь')
        verbose_name_plural = _('Пользователи')
