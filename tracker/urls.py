from django.conf.urls import url, include

from .views import *
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^users/$', UserListView.as_view(), name='users-list'),
    url(r'^users/(?P<pk>\d+)/$', UserDetailView.as_view(), name='user-detail'),
    url(r'^users/(?P<pk>\d+)/create$', UserCreateView.as_view(), name='user-create'),
    url(r'^users/(?P<pk>\d+)/update$', UserUpdateView.as_view(), name='user-update'),
    url(r'^users/(?P<pk>\d+)/delete$', UserDestroyView.as_view(), name='user-delete'),
    url(r'^tasks/$', TaskListView.as_view(), name='tasks-list'),
    url(r'^tasks/(?P<pk>\d+)/$', TaskDetailView.as_view(), name='task-detail'),
    url(r'^tasks/(?P<pk>\d+)/create$', TaskCreateView.as_view(), name='task-create'),
    url(r'^tasks/(?P<pk>\d+)/update$', TaskUpdateView.as_view(), name='task-update'),
    url(r'^tasks/(?P<pk>\d+)/delete$', TaskDestroyView.as_view(), name='task-delete'),
]