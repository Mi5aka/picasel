from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer
from django_filters.rest_framework import DjangoFilterBackend

from .serializers import UserSerializer, TaskSerializer
from .models.users import User
from .models.tasks import Task


class UserListView(generics.ListAPIView):
    renderer_classes = [TemplateHTMLRenderer]
    serializer_class = UserSerializer
    template_name = 'tracker/users_list.html'

    def get(self, request, *args, **kwargs):
        queryset = User.objects.all()
        return Response({'users': queryset})


class UserDetailView(generics.RetrieveAPIView):
    renderer_classes = [TemplateHTMLRenderer]
    serializer_class = UserSerializer
    template_name = 'tracker/user_detail.html'

    def get(self, request, pk):
        user = User.objects.get(pk=pk)
        tasks = Task.objects.filter(user=pk)
        return Response({'user': user, 'tasks': tasks})


class UserCreateView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserUpdateView(generics.UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDestroyView(generics.DestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class TaskListView(generics.ListAPIView):
    serializer_class = TaskSerializer
    renderer_classes = [TemplateHTMLRenderer]
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('name',)
    search_fields = ('name',)
    template_name = 'tracker/tasks_list.html'

    def get(self, request, *args, **kwargs):
        queryset = Task.objects.all()[::-1]
        return Response({'tasks': queryset})


class TaskDetailView(generics.RetrieveAPIView):
    renderer_classes = [TemplateHTMLRenderer]
    serializer_class = TaskSerializer
    template_name = 'tracker/task_detail.html'

    def get(self, request, pk):
        task = Task.objects.get(pk=pk)
        return Response({'task': task})


class TaskCreateView(generics.CreateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class TaskUpdateView(generics.UpdateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class TaskDestroyView(generics.DestroyAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


def index(request):
    return render(request, 'base.html')
