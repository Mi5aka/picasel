### Зависимости
* python 3.6
* docker 17.05
* postgresql >= 9.6

### Pre development
pip install -r ./requirements.txt

### Docker
* Подготовка

Создать файл *.env (см. db.env_example)

Заполнить в следующем виде:
```
POSTGRES_DB=имя_базы
POSTGRES_USER=имя_пользователя
POSTGRES_PASSWORD=пароль
```

Дописать соответствующие настройки в config/settings/database.py

* Запуск: 
```
cd docker /

docker-compose up -d
```

